import { Injectable } from '@angular/core';

//MODEL
import { User } from '../models/user.model'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private api_token: string = null;
  //private isLogged = false;
  private user = new User();

  constructor() { }

  public login(userInfo: User){
    //localStorage.setItem('ACCESS_TOKEN', "access_token");

    this.api_token = userInfo.api_token;

    this.user.id = userInfo.id;
    this.user.email = userInfo.email;
    this.user.identification = userInfo.identification;
  }

  public isLogged(){
    return this.api_token !== null;
  }

  public logout(){
    this.api_token = null;
    //localStorage.removeItem('ACCESS_TOKEN');
  }

  public setAPITOKEN(token: string)
  {
    this.api_token = token;
  }

  public getAPITOKEN()
  {
    return this.api_token;
  }
}
