import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from "rxjs/operators";

//Models
import { Product } from '../models/product.model';
import { Response } from '../models/response.model';

//SERVICES
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private apiRoot: string = 'https://blooming-hamlet-62206.herokuapp.com/api/';

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  getAll() : Observable<Response> { 

    let apiRoot = this.apiRoot + 'products';

    console.log('this.authService.getAPITOKEN()',this.authService.getAPITOKEN());

    const httpOptions = {
      headers: new HttpHeaders({
        //'Content-Type':  'application/json',
        'Authorization': 'Bearer ' + this.authService.getAPITOKEN()
      })
    };
    

    return this.http.get(apiRoot, httpOptions ).pipe(map(res => {

      let response = new Response();
      let resultRAW: any = res;

      //Set response
      response.status = resultRAW.status;
      response.message = resultRAW.message;

      response.result = resultRAW.result?.map(item => {

        let country = new Product();
        country.id = item.id;
        country.name = item.name;
        country.description = item.description;
        country.quantity = item.quantity;
        country.store_id = item.store_id;
        country.created_at = item.created_at;
        country.updated_at = item.updated_at;
        
        return country;
      });

      response.records = resultRAW.result?.length;

      return response;

    }),
      catchError(error => {
        return throwError(error.message);
      }));
  }
}
