import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

//MODEL
import { User } from '../models/user.model';
import { Country } from '../models/country.model';

//SERVICES
import { UtilsService } from '../services/helpers/utils.service';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { CountryService } from '../services/country.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public countries: Array<Country> = [];

  public errorsList: Array<string> = [];

  public userRegister = new User();

  loginForm: FormGroup = this.fb.group({
    country_code: ['', [Validators.required]],
    name: ['', [Validators.required]],
    identification: ['', [Validators.required]],
    email: ['', [Validators.required,Validators.email]],
    password: ['', [Validators.required]],
    c_password: ['', [Validators.required]]
  },
    {
      validator: this.MustMatch('password', 'c_password')
    });


  constructor(
    private router: Router,
    private fb: FormBuilder,
    private utilsService: UtilsService,
    private authService: AuthService,
    private userService: UserService,
    private countryService: CountryService
  ) { }

  ngOnInit() {

    // this.countries.push({ id: '1', country_code: '55', name: 'Brazil' });
    // this.countries.push({ id: '2', country_code: '51', name: 'Perú' });
    this.getAllCountries();
  }

  getAllCountries() {
    this.countryService.getAll().subscribe(response => {

      this.utilsService.openSnackBar(response.message, 'OK');
      //loading.dismiss();
      console.log(response.message);
      if (response.status) {

        this.countries = response.result;
      }

    }, error => {
      console.log(error);
      this.utilsService.openSnackBar(error);
      //loading.dismiss();
    });
  }

  getFormValues() {

    let userRegister = new User();

    userRegister.country_code = this.loginForm.value.country_code;
    userRegister.name = this.loginForm.value.identification;
    userRegister.email = this.loginForm.value.email;
    userRegister.identification = this.loginForm.value.identification;
    userRegister.password = this.loginForm.value.password;
    userRegister.c_password = this.loginForm.value.c_password;

    console.log('userRegister', userRegister);

    return userRegister;
  }

  signUp() {
    console.log('register...');
  }

  register() {

    if (this.loginForm.invalid) {
      this.errorsList = this.utilsService.getFormError(this.loginForm);
      if(this.errorsList.length > 0)
      this.utilsService.openSnackBar(this.errorsList[0]);
      return;
    }

    this.userRegister = this.getFormValues();

    this.userService.register(this.userRegister).subscribe(response => {

      this.utilsService.openSnackBar(response.message, 'OK');
      //loading.dismiss();
      console.log(response.message);
      if (response.status) {
        this.router.navigate(['/login']);
      }

    }, error => {
      console.log(error);
      this.utilsService.openSnackBar(error);
      //loading.dismiss();
    });
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }
}
