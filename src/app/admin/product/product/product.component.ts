import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { SelectionModel } from '@angular/cdk/collections';

//MODEL
import { Product } from '../../../models/product.model';

//SERVICES
import { UtilsService } from '../../../services/helpers/utils.service'
import { ProductService } from '../../../services/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  displayedColumns = ['select', 'id', 'name', 'description'];
  dataSource: MatTableDataSource<Product>;
  selection: SelectionModel<Product>;

  public products: Array<Product> = [];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(
    private productService: ProductService,
    private utilsService: UtilsService
    ) {}

  ngOnInit() {
    // this.dataSource = new MatTableDataSource(this.productService.getAll());
    this.selection = new SelectionModel<Product>(true, []);
    this.getAll(); 
  }

  getAll()
  {
    this.productService.getAll().subscribe(response => {

      this.utilsService.openSnackBar(response.message, 'OK');
      //loading.dismiss();
      console.log(response.message);
      if (response.status) {

        this.products = response.result;
        this.dataSource = new MatTableDataSource(this.products);

        // ngAfterViewInit()
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }

    }, error => {
      console.log(error);
      this.utilsService.openSnackBar(error);
      //loading.dismiss();
    });
  }

  // ngAfterViewInit() {
    
  // }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach(row => this.selection.select(row));
  }
}
