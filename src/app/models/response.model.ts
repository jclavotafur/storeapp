export class Response {

    public status: boolean = false;
    public message: string = null;
    public result: any;
    public records: number = 0;

}