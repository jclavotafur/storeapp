export class Product {
    id: string;
    name: string;
    description: string;
    quantity: string;
    store_id: string;
    created_at: string; 
    updated_at: string;    
}