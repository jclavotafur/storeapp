export class Country {
    id: string;
    country_code: string;
    name: string;
}