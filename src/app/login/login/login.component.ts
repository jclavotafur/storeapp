import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

//MODEL
import { User } from '../../models/user.model';
import { Country } from '../../models/country.model';

//SERVICES
import { UtilsService } from '../../services/helpers/utils.service'
import { AuthService } from '../../services/auth.service'
import { UserService } from '../../services/user.service'
import { CountryService } from '../../services/country.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public countries: Array<Country> = [];

  public errorsList: Array<string> = [];

  public userLogin = new User();

  loginForm: FormGroup = this.fb.group({
    country_code: ['', [Validators.required]],
    identification: ['', [Validators.required]],
    password: ['', [Validators.required]]
    // password: ['', [Validators.required, Validators.minLength(8)]]
  });


  constructor(
    private router: Router,
    private fb: FormBuilder,
    private utilsService: UtilsService,
    private authService: AuthService,
    private userService: UserService,
    private countryService: CountryService
  ) { }

  ngOnInit() {

    // this.countries.push({ id: '1', country_code: '55', name: 'Brazil' });
    // this.countries.push({ id: '2', country_code: '51', name: 'Perú' });
    this.getAllCountries();
  }

  getAllCountries()
  {
    this.countryService.getAll().subscribe(response => {

      this.utilsService.openSnackBar(response.message, 'OK');
      //loading.dismiss();
      console.log(response.message);
      if (response.status) {

        this.countries = response.result;
      }

    }, error => {
      console.log(error);
      this.utilsService.openSnackBar(error);
      //loading.dismiss();
    });
  }

  getFormValues() {

    let userLogin = new User();

    userLogin.id = this.loginForm.value.id;
    userLogin.country_code = this.loginForm.value.country_code;
    userLogin.identification = this.loginForm.value.identification;
    userLogin.password = this.loginForm.value.password;

    console.log('userLogin', userLogin);

    return userLogin;
  }

  signUp() {
    console.log('register...');
  }

  login() {

    if (this.loginForm.invalid) {
      this.utilsService.getFormError(this.loginForm);
      return;
    }

    this.userLogin = this.getFormValues();

    this.userService.login(this.userLogin).subscribe(response => {

      this.utilsService.openSnackBar(response.message, 'OK');
      //loading.dismiss();
      console.log(response.message);
      if (response.status) {

        this.authService.login(response.result);
        this.router.navigate(['/dashboard']);
      }

    }, error => {
      console.log(error);
      this.utilsService.openSnackBar(error);
      //loading.dismiss();
    });
  }

}

